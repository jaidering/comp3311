-- Q1:  which rooms have a given facility

create or replace function
	Q1(text) returns setof FacilityRecord
as $$
select r.longname, f.description
from Rooms r
     join RoomFacilities rf on (rf.room = r.id)
     join Facilities f on (rf.facility = f.id)
where f.description ilike '%'||$1||'%';
$$ language sql;

-- Q2: semester containing a particular day

create or replace function
	Term(integer) returns text
as $$
select substr(year::text,3,2)||lower(sess)
from Terms
where id = $1
$$ language sql;

create or replace function Q2(_day date) returns text 
as $$
declare
	startDate date;     
	endDate date;     
	nextStart date;
	prevEnd date;     
	currTerm integer; 
	prevTerm integer; 
	nextTerm integer; 
    matchingTerm integer;  
    termHolidays interval; 
begin
	-- outside semesters
	select into startDate min(starting) from Terms;
	select into endDate max(ending) from Terms;
	if (_day < startDate or _day > endDate) then
		return null;
	end if;
	-- within semester
	select id into matchingTerm
    from Terms
    where _day between starting and ending;
	if (matchingTerm is not null) then
		return Term(matchingTerm);
	end if;
	-- not in a Term, find terms around given date
	select id, ending into prevTerm, prevEnd from Terms
	where ending = (select max(ending) from Terms where ending < _day);
	select id, starting into nextTerm, nextStart from Terms
	where starting = (select min(starting) from Terms where starting > _day);
	termHolidays := nextStart::timestamp - prevEnd::timestamp;
	if (termHolidays < '1 week') then
		nextStart := (prevEnd::timestamp + '1 day')::date;
	else
		nextStart := (nextStart::timestamp - interval '1 week')::date;
		prevEnd := (nextStart::timestamp - interval '1 day')::date;
	end if;
	if (_day <= prevEnd::date) then
		return Term(prevTerm);
	elsif (_day >= nextStart::date) then
		return Term(nextTerm);
	else
		return ' ';
	end if;
end;
$$ language plpgsql;

-- Q3: transcript with variations

create or replace function
	q3(_sid integer) returns setof TranscriptRecord
as $$
declare
	rec TranscriptRecord;
	var record;
	subj Subjects;
	UOCtotal integer := 0;
	UOCpassed integer := 0;
	UOCadvanced integer := 0;
	overallWam integer := 0;
	wam integer := 0;
	stu_id integer;
begin
	select s.id into stu_id
	from Students s join People p on (p.id = s.id)
	where p.unswid = _sid;
	for rec in
		select s.code, substr(t.year::text,3,2)||lower(t.sess), s.name, e.mark, e.grade, s.uoc
		from CourseEnrolments e, Courses c, Subjects s, Terms t
		where e.student = stu_id and e.course = c.id and c.subject = s.id and c.term = t.id
		order by t.starting, s.code
	loop
		if (rec.grade = 'SY') then
			UOCpassed := UOCpassed + rec.uoc;
		elsif (rec.mark is not null) then
			if (rec.grade in ('PT', 'PC', 'PS', 'CR', 'DN', 'HD')) then
		-- only counts towards UOC if they passed the course
				UOCpassed := UOCpassed + rec.uoc;
			end if;
			-- WAM calculation for fails
			UOCtotal := UOCtotal + rec.uoc;
			overallWam := overallWam + (rec.mark * rec.uoc);
		end if;
		return next rec;
	end loop;
	UOCadvanced := 0;
	for var in
		select s.id as subject, s.code, s.uoc, v.vtype, v.intequiv, v.extequiv
		from Variations v join Subjects s on (v.subject = s.id)
		where v.student = stu_id
		order by s.code 
	loop
		-- possibilities: advstanding, substitution, exemption
		rec = (var.code, null, null, null, null, null);
		if (var.vtype = 'advstanding') then
			UOCadvanced := UOCadvanced + var.uoc;
			rec.name := 'Advanced standing, due to : ';
			rec.uoc  := var.uoc;
		elsif (var.vtype = 'substitution') then
			rec.name := 'Substitution, due to : ';
		else
			rec.name := 'Exemption, due to : ';
		end if;
		return next rec;
		-- possibilities: internal/external subject
		rec.code := null; rec.uoc := null;
		if (var.intequiv is not null) then
			select 'studying '||code||' at UNSW' into rec.name
			from Subjects where id = var.intequiv;
		else
			select 'study at '||institution into rec.name
			from ExternalSubjects where id = var.extequiv;
		end if;
		return next rec;
	end loop;
	if (UOCtotal = 0) then
		rec := (null, null, 'No WAM to show', null, null, null);
	else
		wam := overallWam / UOCtotal;
		rec := (null, null, 'Overall WAM', wam, null, UOCpassed + UOCadvanced);
	end if;
	return next rec;
end;
$$ language plpgsql;
